package com.jconnerprototype.jconnerprototypespring.Models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestWidgetRequest {
    private String requestId;
    private String name;
    private String valueOne;
    private String valueTwo;
    public final String type = "TestWidgetRequest";
}
