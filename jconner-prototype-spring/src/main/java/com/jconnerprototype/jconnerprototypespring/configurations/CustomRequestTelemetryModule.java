package com.jconnerprototype.jconnerprototypespring.configurations;


import com.microsoft.applicationinsights.TelemetryConfiguration;
import com.microsoft.applicationinsights.core.dependencies.apachecommons.lang3.StringUtils;
import com.microsoft.applicationinsights.extensibility.TelemetryModule;
import com.microsoft.applicationinsights.telemetry.RequestTelemetry;
import com.microsoft.applicationinsights.web.extensibility.modules.WebTelemetryModule;
import com.microsoft.applicationinsights.web.internal.RequestTelemetryContext;
import com.microsoft.applicationinsights.web.internal.ThreadContext;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Map;
import java.util.stream.Collectors;

public class CustomRequestTelemetryModule implements WebTelemetryModule, TelemetryModule {
    @Override
    public void initialize(TelemetryConfiguration telemetryConfiguration) {

    }

    @Override
    public void onBeginRequest(ServletRequest servletRequest, ServletResponse servletResponse) {

        RequestTelemetryContext context = ThreadContext.getRequestTelemetryContext();
        RequestTelemetry requestTelemetry = context.getHttpRequestTelemetry();
        //String body = requestBodyGet(servletRequest);


        String body = null;
        try {
            body = getRequestData((HttpServletRequest) servletRequest);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        requestTelemetry.getProperties().put("requestBody", body);

    }

    @Override
    public void onEndRequest(ServletRequest servletRequest, ServletResponse servletResponse) {

    }
/*
    private String requestBodyGet(ServletRequest servletRequest) {
        String body = "";
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
            ContentCachingRequestWrapper requestToCache = new ContentCachingRequestWrapper(httpRequest);
            try {
                InputStream inputStream = requestToCache.getInputStream();
                StringBuilder stringBuilder = new StringBuilder();
                if (inputStream != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    char[] charBuffer = new char[128];
                    int bytesRead;
                    while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                        stringBuilder.append(charBuffer, 0, bytesRead);
                    }
                } else {
                }
                body = stringBuilder.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //body = extractRequestPayload(httpRequest);

        }
        return body;
    }*/

    private String getRequestData(HttpServletRequest request) throws UnsupportedEncodingException {
        String payload = null;
        //ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        ContentCachingRequestWrapper wrapper = new ContentCachingRequestWrapper(request);
        Map<String,String[]> paramMap = wrapper.getParameterMap();
        if (wrapper != null) {
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
            }
        }
        return payload;
    }

    private String getResponseData(HttpServletResponse response) throws IOException {
        String payload = null;
        ContentCachingResponseWrapper wrapper =
                WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
        if (wrapper != null) {
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
                wrapper.copyBodyToResponse();
            }
        }
        return payload;
    }

    private String extractRequestPayload(HttpServletRequest request) {
        ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);

        if (wrapper == null) {
            return "";
        }

        if (wrapper.getContentLength() > 0) {
            try {
                return request.getReader().lines().collect(Collectors.joining(StringUtils.LF));
            } catch (IOException e) {
                return "";
            }
        }
        return "";
    }
}
