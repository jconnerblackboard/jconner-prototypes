package com.jconnerprototype.jconnerprototypespring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JconnerPrototypeSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(JconnerPrototypeSpringApplication.class, args);
	}

}
