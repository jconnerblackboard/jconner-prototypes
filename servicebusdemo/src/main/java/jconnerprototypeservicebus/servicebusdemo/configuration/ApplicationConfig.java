package jconnerprototypeservicebus.servicebusdemo.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.microsoft.azure.servicebus.QueueClient;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import jconnerprototypeservicebus.servicebusdemo.handlers.ReceiverMessageHandler;
import jconnerprototypeservicebus.servicebusdemo.handlers.ReceiverSessionHandler;
import jconnerprototypeservicebus.servicebusdemo.utility.MessageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ApplicationConfig implements WebMvcConfigurer {

    @Autowired
    private ConfigProperties properties;

    @Autowired
    private ServiceBusConfigProperties servicebusProperties;

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.OBJECT_AND_NON_CONCRETE);
        return mapper;
    }

    @Bean
    //@ConditionalOnMissingBean
    public QueueClient responseQueueClient() throws InterruptedException, ServiceBusException {
        return new QueueClient(new ConnectionStringBuilder(servicebusProperties.getConnectionString(),
                properties.getServiceBusResponseQueue()), servicebusProperties.getQueueReceiveMode());
    }

    @Bean
    //@ConditionalOnMissingBean
    public QueueClient requestQueueClient() throws InterruptedException, ServiceBusException {
        return new QueueClient(new ConnectionStringBuilder(servicebusProperties.getConnectionString(),
                properties.getServiceBusRequestQueue()), servicebusProperties.getQueueReceiveMode());
    }

    @Bean
    //@ConditionalOnMissingBean
    public QueueClient responseQueueSessionedClient() throws InterruptedException, ServiceBusException {
        return new QueueClient(new ConnectionStringBuilder(servicebusProperties.getConnectionString(),
                properties.getServiceBusResponseQueueSessioned()), servicebusProperties.getQueueReceiveMode());
    }

    @Bean
    //@ConditionalOnMissingBean
    public QueueClient requestQueueSessionedClient() throws InterruptedException, ServiceBusException {
        return new QueueClient(new ConnectionStringBuilder(servicebusProperties.getConnectionString(),
                properties.getServiceBusRequestQueueSessioned()), servicebusProperties.getQueueReceiveMode());
    }

    @Bean
    public ReceiverMessageHandler receiverMessageHandler()    {
        return new ReceiverMessageHandler();
    }

    @Bean
    public ReceiverSessionHandler receiverSessionHandler(){
        return new ReceiverSessionHandler();
    }

    @Bean
    public MessageUtility messageUtility(){
        return new MessageUtility();
    }
}
