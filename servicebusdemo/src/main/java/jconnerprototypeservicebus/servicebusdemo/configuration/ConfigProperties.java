package jconnerprototypeservicebus.servicebusdemo.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "servicebusdemo")
public class ConfigProperties {
    private String serviceBusRequestQueue;
    private String serviceBusResponseQueue;
    private String serviceBusRequestQueueSessioned;
    private String serviceBusResponseQueueSessioned;
}
