package jconnerprototypeservicebus.servicebusdemo.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.servicebus.ExceptionPhase;
import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.IMessageSession;
import com.microsoft.azure.servicebus.ISessionHandler;
import com.microsoft.azure.servicebus.Message;
import com.microsoft.azure.servicebus.QueueClient;
import jconnerprototypeservicebus.servicebusdemo.utility.MessageUtility;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Log4j2
public class ReceiverSessionHandler implements ISessionHandler {

    @Autowired
    private MessageUtility messageUtility;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverSessionHandler.class);

    @Autowired
    @Qualifier("requestQueueSessionedClient")
    private QueueClient requestClient;

    @Autowired
    @Qualifier("responseQueueSessionedClient")
    private QueueClient responseClient;

    @Override
    public CompletableFuture<Void> onMessageAsync(IMessageSession session,
            IMessage message) {
        // Debugging
        messageUtility.logReceivedMessage(message);

        Message replyMessage = messageUtility.extractReplyMessage(message);
        replyMessage.setSessionId(message.getSessionId());

        responseClient.sendAsync(replyMessage);

        return CompletableFuture.completedFuture(null);
    }

    @Override
    public CompletableFuture<Void> OnCloseSessionAsync(IMessageSession session) {
        return CompletableFuture.completedFuture(null);
    }

    @Override
    public void notifyException(Throwable exception,
            ExceptionPhase phase) {
        LOGGER.error("eeks!", exception);
    }
}
