package jconnerprototypeservicebus.servicebusdemo.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.servicebus.ExceptionPhase;
import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.IMessageHandler;
import com.microsoft.azure.servicebus.Message;
import com.microsoft.azure.servicebus.QueueClient;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import jconnerprototypeservicebus.servicebusdemo.utility.MessageUtility;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Log4j2
public class ReceiverMessageHandler implements IMessageHandler {

    @Autowired
    private MessageUtility messageUtility;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverMessageHandler.class);

    @Autowired
    @Qualifier("requestQueueClient")
    private QueueClient requestClient;

    @Autowired
    @Qualifier("responseQueueClient")
    private QueueClient responseClient;


    @Autowired
    @Qualifier("responseQueueSessionedClient")
    private QueueClient responseSessionedClient;

    @Override
    public CompletableFuture<Void> onMessageAsync(IMessage message) {
        // Debugging
        messageUtility.logReceivedMessage(message);

        Message replyMessage = messageUtility.extractReplyMessage(message);
        replyMessage.setCorrelationId(message.getMessageId());
        replyMessage.setSessionId(message.getSessionId());

        responseClient.sendAsync(replyMessage)
                .thenRunAsync(() -> System.out.printf("\n\tMessage sent: Id = %s", message.getMessageId()));

        try {
            responseSessionedClient.send(replyMessage);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ServiceBusException e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    @Override
    public void notifyException(Throwable exception,
            ExceptionPhase phase) {
        LOGGER.error("eeks!", exception);
    }
}
