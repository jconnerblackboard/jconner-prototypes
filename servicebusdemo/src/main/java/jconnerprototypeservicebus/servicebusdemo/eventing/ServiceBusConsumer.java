package jconnerprototypeservicebus.servicebusdemo.eventing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.servicebus.ExceptionPhase;
import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.IMessageHandler;
import com.microsoft.azure.servicebus.ISubscriptionClient;
import com.microsoft.azure.servicebus.MessageHandlerOptions;
import com.microsoft.azure.servicebus.QueueClient;
import com.microsoft.azure.servicebus.SessionHandlerOptions;
import jconnerprototypeservicebus.servicebusdemo.utility.MessageUtility;
import jconnerprototypeservicebus.servicebusdemo.handlers.ReceiverMessageHandler;
import jconnerprototypeservicebus.servicebusdemo.handlers.ReceiverSessionHandler;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

@Log4j2
@Component
public class ServiceBusConsumer implements Ordered {

    @Autowired
    @Qualifier("requestQueueClient")
    private QueueClient requestClient;

    @Autowired
    @Qualifier("responseQueueClient")
    private QueueClient responseClient;

    @Autowired
    @Qualifier("requestQueueSessionedClient")
    private QueueClient requestSessionedClient;

    @Autowired
    @Qualifier("responseQueueSessionedClient")
    private QueueClient responseSessionedClient;

    @Autowired
    private ReceiverMessageHandler receiverMessageHandler;

    @Autowired
    private ReceiverSessionHandler receiverSessionHandler;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MessageUtility messageUtility;

    private final ISubscriptionClient iSubscriptionClient;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceBusConsumer.class);

    ServiceBusConsumer(ISubscriptionClient isc) {
        this.iSubscriptionClient = isc;
    }
/*
    @EventListener(ApplicationReadyEvent.class)
    public void consume() throws Exception {

        this.iSubscriptionClient.registerMessageHandler(new IMessageHandler() {

            @Override
            public CompletableFuture<Void> onMessageAsync(IMessage message) {

                messageUtility.logReceivedMessage(message);
                return CompletableFuture.completedFuture(null);
            }

            @Override
            public void notifyException(Throwable exception,
                    ExceptionPhase phase) {
                LOGGER.error("eeks!", exception);
            }
        }, Executors.newCachedThreadPool());

    }

 */

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    /*
    @EventListener(ApplicationReadyEvent.class)
    public void startReceiver() throws Exception {
        requestClient.registerMessageHandler(receiverMessageHandler,
                new MessageHandlerOptions(4, true, Duration.ofMinutes(1)), Executors.newCachedThreadPool());
    }

     */

    @EventListener(ApplicationReadyEvent.class)
    public void startSessionedReceiver() throws Exception {
        log.info("***************  Starting Sessioned Receiver **********************");
        requestSessionedClient.registerSessionHandler(receiverSessionHandler,
                new SessionHandlerOptions(5, true, Duration.ofMinutes(1)), Executors.newCachedThreadPool());
    }
}
