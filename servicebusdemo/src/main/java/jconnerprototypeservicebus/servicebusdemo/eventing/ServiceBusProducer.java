package jconnerprototypeservicebus.servicebusdemo.eventing;

import static jconnerprototypeservicebus.servicebusdemo.constants.Constants.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.servicebus.ClientFactory;
import com.microsoft.azure.servicebus.ClientSettings;
import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.IMessageSession;
import com.microsoft.azure.servicebus.ITopicClient;
import com.microsoft.azure.servicebus.Message;
import com.microsoft.azure.servicebus.MessageSession;
import com.microsoft.azure.servicebus.QueueClient;
import com.microsoft.azure.servicebus.ReceiveMode;
import com.microsoft.azure.servicebus.primitives.MessagingEntityType;
import com.microsoft.azure.servicebus.primitives.MessagingFactory;
import com.microsoft.azure.servicebus.primitives.RetryPolicy;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import com.microsoft.azure.servicebus.security.TokenProvider;
import jconnerprototypeservicebus.servicebusdemo.configuration.ServiceBusConfigProperties;
import jconnerprototypeservicebus.servicebusdemo.domain.BalanceInquiryRequest;
import jconnerprototypeservicebus.servicebusdemo.utility.MessageUtility;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.support.ManagedMap;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.time.Duration;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Log4j2
@Component
public class ServiceBusProducer implements Ordered {

    @Autowired
    private MessageUtility messageUtility;

    @Autowired
    private ServiceBusConfigProperties servicebusProperties;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    @Qualifier("requestQueueClient")
    private QueueClient requestClient;

    @Autowired
    @Qualifier("responseQueueClient")
    private QueueClient responseClient;

    @Autowired
    @Qualifier("responseQueueSessionedClient")
    private QueueClient responseClientSessioned;

    @Autowired
    @Qualifier("requestQueueSessionedClient")
    private QueueClient requestSessionedClient;

    private final ITopicClient iTopicClient;

    ServiceBusProducer(ITopicClient iTopicClient) {
        this.iTopicClient = iTopicClient;
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void produce() throws Exception {
        sendSessionedMessage();
        sendSessionedMessage(messageUtility.buildMessage(UUID.randomUUID().toString()));
        sendSessionedMessageAsync(messageUtility.buildMessage(UUID.randomUUID().toString()));
    }

    private void sendSessionedMessage() throws JsonProcessingException {
        String sessionId1 = new String("Session1_" + UUID.randomUUID().toString());
        String sessionId2 = new String("Session2_" + UUID.randomUUID().toString());

        Message message1 = messageUtility.buildMessage(sessionId1);
        Message message2 = messageUtility.buildMessage(sessionId2);

        log.info("************** Sending sessioned message with id: " + sessionId1);
        requestSessionedClient.sendAsync(message1);

        log.info("************** Sending sessioned message with id: " + sessionId2);
        requestSessionedClient.sendAsync(message2);

        try {
            String connectionString =
                    servicebusProperties.getConnectionString() + ";" + SESSIONED_RESPONSE_QUEUE_ENTITY_PATH_FULL;

            log.info("************** Starting session listener for session id: " + sessionId2);
            IMessageSession messageSession = ClientFactory
                    .acceptSessionFromConnectionString(connectionString, sessionId2);

            log.info("********** Waiting for reply message *********");

            int retryCount = 0;

            while (retryCount < MAX_SESSION_READ_RETRY_COUNT) {
                IMessage replyMessage = messageSession
                        .receive(Duration.ofSeconds(MAX_SESSION_WAIT_DURATION_IN_SECONDS));
                if (replyMessage != null) {
                    log.info("*************** Received reply message with session id: " + replyMessage.getSessionId());
                    messageUtility.logReceivedMessage(replyMessage);
                    break;
                }
                retryCount++;
                Thread.sleep(3000);
            }

            messageSession.close();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ServiceBusException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send a sessioned message and sit on the queue for a response.
     * @param message
     */
    private void sendSessionedMessage(Message message) {

        if (message.getSessionId() == null) {
            message.setSessionId(UUID.randomUUID().toString());
        }
        requestSessionedClient.sendAsync(message);

        try {
            String connectionString =
                    servicebusProperties.getConnectionString() + ";" + SESSIONED_RESPONSE_QUEUE_ENTITY_PATH_FULL;

            IMessageSession messageSession = ClientFactory
                    .acceptSessionFromConnectionString(connectionString, message.getSessionId());

            int retryCount = 0;
            while (retryCount < MAX_SESSION_READ_RETRY_COUNT) {
                IMessage replyMessage = messageSession
                        .receive(Duration.ofSeconds(MAX_SESSION_WAIT_DURATION_IN_SECONDS));
                if (replyMessage != null) {
                    messageUtility.logReceivedMessage(replyMessage);
                    break;
                }
                retryCount++;
                Thread.sleep(3000);
            }

            messageSession.close();

        } catch (InterruptedException e) {
            log.error(e);
        } catch (ServiceBusException e) {
            log.error(e);
        }
    }

    private void sendSessionedMessageAsync(Message message) {

        if (message.getSessionId() == null) {
            message.setSessionId(UUID.randomUUID().toString());
        }
        requestSessionedClient.sendAsync(message);

        try {
            String connectionString =
                    servicebusProperties.getConnectionString() + ";" + SESSIONED_RESPONSE_QUEUE_ENTITY_PATH_FULL;

            CompletableFuture<IMessageSession> future = ClientFactory
                    .acceptSessionFromConnectionStringAsync(connectionString, message.getSessionId());

            IMessageSession messageSession = future.get();

            int retryCount = 0;
            while (retryCount < MAX_SESSION_READ_RETRY_COUNT) {
                IMessage replyMessage =  messageSession
                        .receiveAsync(Duration.ofSeconds(MAX_SESSION_WAIT_DURATION_IN_SECONDS)).get();
                if (replyMessage != null) {
                    messageUtility.logReceivedMessage(replyMessage);
                    break;
                }
                retryCount++;
                Thread.sleep(3000);
            }

            messageSession.close();

        } catch (InterruptedException e) {
            log.error(e);
        } catch (ServiceBusException e) {
            log.error(e);
        } catch (ExecutionException e) {
            log.error(e);
        }
    }
}
