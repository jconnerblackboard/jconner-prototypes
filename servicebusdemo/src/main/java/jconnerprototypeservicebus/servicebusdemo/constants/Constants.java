package jconnerprototypeservicebus.servicebusdemo.constants;

public class Constants {
    private Constants(){}

    public static final String SESSIONED_RESPONSE_QUEUE_NAMESPACE = "apollo-dev-jconner-servicebus";
    public static final String SESSIONED_RESPONSE_QUEUE_ENTITY_PATH = "apollo-dev-jconner-servicebus-response-queue-sessioned";
    public static final String SESSIONED_RESPONSE_QUEUE_ENTITY_PATH_FULL = "EntityPath=" + SESSIONED_RESPONSE_QUEUE_ENTITY_PATH;
    public static final int MAX_SESSION_READ_RETRY_COUNT = 3;
    public static final int MAX_SESSION_WAIT_DURATION_IN_SECONDS = 30;


}
