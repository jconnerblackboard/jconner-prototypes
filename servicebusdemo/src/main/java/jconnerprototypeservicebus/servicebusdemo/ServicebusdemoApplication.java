package jconnerprototypeservicebus.servicebusdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicebusdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicebusdemoApplication.class, args);
	}

}
