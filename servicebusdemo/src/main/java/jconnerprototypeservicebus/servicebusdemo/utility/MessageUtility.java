package jconnerprototypeservicebus.servicebusdemo.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.Message;
import jconnerprototypeservicebus.servicebusdemo.domain.AccountBalance;
import jconnerprototypeservicebus.servicebusdemo.domain.BalanceInquiryRequest;
import jconnerprototypeservicebus.servicebusdemo.domain.BalanceInquiryResponse;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.ManagedMap;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Log4j2
public class MessageUtility {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageUtility.class);

    public void logReceivedMessage(IMessage message) {
        StringBuilder builder = new StringBuilder();
        message.getProperties().forEach((k, v) -> builder.append((k + ":" + v + "\n")));
        String receivedMessage =
                "\r\nReceived: [" + message.getSessionId() + "]: \n" + "Id: " + message.getMessageId() + "\n" + "Body: "
                        + new String(message.getBody()) + "\n" + "Properties:\n" + builder.toString();
        log.info(receivedMessage);
    }

    public Message extractReplyMessage(IMessage message) {
        try {
            String body = new String(message.getBody());
            BalanceInquiryRequest balanceInquiryRequest = mapper.readValue(body, BalanceInquiryRequest.class);

            BalanceInquiryResponse balanceInquiryResponse = BalanceInquiryResponse.builder().
                    accountBalances(List.of(AccountBalance.builder().
                            balance("100.00").
                            openToBuyBalance("100.00").
                            accountId(UUID.randomUUID().toString()).
                            accountTypeId(UUID.randomUUID().toString()).build())).
                    accountHolderId(balanceInquiryRequest.getAccountHolderId()).
                    institutionId(balanceInquiryRequest.getInstitutionId()).requestResult("OK").build();

            return new Message(mapper.writeValueAsBytes(balanceInquiryResponse));

        } catch (IOException e) {
            log.error("Error occurred while converting json String to java object ", e);
            System.out.println("Error occurred while converting json String to java object ");
        }
        return null;
    }

    public Message buildMessage(String sessionId) throws JsonProcessingException {
        BalanceInquiryRequest request = BalanceInquiryRequest.builder().accountHolderId(UUID.randomUUID().toString())
                .institutionId(UUID.randomUUID().toString()).build();
        Message message = new Message(mapper.writeValueAsBytes(request));
        message.setLabel("AuthEngineBalanceInquiryRequest");
        message.setContentType("application/json");

        Map<String, String> properties = new ManagedMap<>();
        properties.putIfAbsent("eventType", "AuthEngineBalanceInquiryRequest");
        properties.putIfAbsent("institutionId", "9c83f5bb-d1c6-4772-abf5-7f38bb934f6b");
        properties.putIfAbsent("version", "1.0");

        message.setProperties(properties);
        message.setSessionId(sessionId);
        return message;
    }
}
