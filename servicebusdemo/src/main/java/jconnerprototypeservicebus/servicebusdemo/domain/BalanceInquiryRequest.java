package jconnerprototypeservicebus.servicebusdemo.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BalanceInquiryRequest implements Serializable {
    private String accountHolderId;
    private String institutionId;
}
