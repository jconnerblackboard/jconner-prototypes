package jconnerprototypeservicebus.servicebusdemo.domain;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BalanceInquiryResponse implements Serializable {
    private String institutionId;
    private String accountHolderId;
    private String requestResult;
    private List<AccountBalance> accountBalances;
}
