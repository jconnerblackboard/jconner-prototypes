package jconnerprototypeservicebus.servicebusdemo.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountBalance implements Serializable {
    private String accountTypeId;
    private String accountId;
    private String balance;
    private String openToBuyBalance;
}
