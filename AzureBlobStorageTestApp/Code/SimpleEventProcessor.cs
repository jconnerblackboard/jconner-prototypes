﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace AzureBlobStorageTestApp.Code
{
    public class SimpleEventProcessor : IEventProcessor
    {
        /// <summary>
        /// Open asychronous communication.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task OpenAsync(PartitionContext context)
        {
            var message = $"Event Processor initialized.  Partition: '{context.Lease.PartitionId}', Offset: '{context.Lease.Offset}'";
            MainForm.FireMessage(message);
            Telemetry.Instance.TrackEvent(message);
            return Task.FromResult<object>(null);
        }

        /// <summary>
        /// Process events asynchronously.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="messages"></param>
        /// <returns></returns>
        public async Task ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages)
        {
            foreach (var eventData in messages)
            {
                var data = Encoding.UTF8.GetString(eventData.GetBytes());
                var message = $"Message received.  Partition: '{context.Lease.PartitionId}', Data: '{data}'";
                MainForm.FireMessage(message);
                Telemetry.Instance.TrackEvent(message);
            }

            await context.CheckpointAsync();
        }

        /// <summary>
        /// Close asychronous communication.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        public async Task CloseAsync(PartitionContext context, CloseReason reason)
        {
            var message = $"Event Processor shutting down.  Partition: '{context.Lease.PartitionId}', Reason: '{reason}'";
            MainForm.FireMessage(message);
            Telemetry.Instance.TrackEvent(message);
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync();
            }
        }
    }
}
