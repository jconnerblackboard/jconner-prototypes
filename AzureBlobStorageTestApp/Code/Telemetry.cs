﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using BbTS.Core.Configuration;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;

namespace AzureBlobStorageTestApp.Code
{
    public class Telemetry
    {
        private string TelemetryKey => ApplicationConfiguration.GetKeyValueAsString("TelemetryKey");

        public static Telemetry Instance = new Telemetry();
        private readonly TelemetryClient _telemetry;

        public Telemetry()
        {
            _telemetry = GetAppInsightsClient();
        }

        public bool Enabled { get; set; } = true;

        private TelemetryClient GetAppInsightsClient()
        {
            var config = new TelemetryConfiguration
            {
                InstrumentationKey = TelemetryKey,
                TelemetryChannel = new Microsoft.ApplicationInsights.WindowsServer.TelemetryChannel.ServerTelemetryChannel { DeveloperMode = Debugger.IsAttached }
            };
            //config.TelemetryChannel = new Microsoft.ApplicationInsights.Channel.InMemoryChannel(); // Default channel
#if DEBUG
            config.TelemetryChannel.DeveloperMode = true;
#endif
            var client = new TelemetryClient(config);
            client.Context.Component.Version = Assembly.GetEntryAssembly().GetName().Version.ToString();
            client.Context.Session.Id = Guid.NewGuid().ToString();
            client.Context.User.Id = (Environment.UserName + Environment.MachineName).GetHashCode().ToString();
            client.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
            return client;
        }

        public void SetUser(string user)
        {
            _telemetry.Context.User.AuthenticatedUserId = user;
        }

        public void TrackEvent(string key, IDictionary<string, string> properties = null, IDictionary<string, double> metrics = null)
        {
            if (Enabled)
            {
                _telemetry.TrackEvent(key, properties, metrics);
            }
        }

        public void TrackException(Exception ex)
        {
            if (ex != null && Enabled)
            {
                var telex = new Microsoft.ApplicationInsights.DataContracts.ExceptionTelemetry(ex);
                _telemetry.TrackException(telex);
                Flush();
            }
        }

        internal void Flush()
        {
            _telemetry.Flush();
        }
    }
}
