﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AzureBlobStorageTestApp.Code;
using BbTS.Core.Configuration;
using BbTS.Core.Conversion;
using Microsoft.ServiceBus.Messaging;

namespace AzureBlobStorageTestApp
{
    public delegate void MessageEvent(string message);

    public partial class MainForm : Form
    {
        private delegate void AppendMessage(string message);
        public static event MessageEvent MessageEvent;

        public static bool KeepRunning { get; set; } = false;

        public static string EventHubConnectionString { get; set; } = ApplicationConfiguration.GetKeyValueAsString("EventHubConnectionString");
        
        public static string EventHubName { get; set; } = ApplicationConfiguration.GetKeyValueAsString("EventHubName");
        public static string StorageAccountName { get; set; } = ApplicationConfiguration.GetKeyValueAsString("StorageAccountName");
        public static string StorageAccountKey { get; set; } = ApplicationConfiguration.GetKeyValueAsString("StorageAccountKey");

        public MainForm()
        {
            InitializeComponent();
            MessageEvent += MainForm_MessageEvent;
        }

        #region Console message handling

        /// <summary>
        /// Handle message events.
        /// </summary>
        /// <param name="message"></param>
        private void MainForm_MessageEvent(string message)
        {
            _appendMessage($"{message}\r\n");
        }

        /// <summary>
        /// Fire a Message Event
        /// </summary>
        /// <param name="message"></param>
        public static void FireMessage(String message)
        {
            MessageEvent?.Invoke(message);
        }

        /// <summary>
        /// Handle populate TS treeview callback events
        /// </summary>
        private void _appendMessage(string message)
        {
            try
            {
                if (ConsoleTextBox.InvokeRequired)
                {
                    AppendMessage d = _appendMessage;
                    Invoke(d, new object[] { message });
                }
                else
                {
                    ConsoleTextBox.AppendText(message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Formatting.FormatException(ex));
            }
        }

        #endregion

        /// <summary>
        /// Handle button clicks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, EventArgs e)
        {
            var buttonName = ((Button)sender).Name;

            switch (buttonName)
            {
                case "StartButton":
                {
                    if (StartButton.Text == @"Start")
                    {
                        KeepRunning = true;
                        StartButton.Text = @"Stop";
                        ThreadPool.QueueUserWorkItem(state => _runListener(), null);
                    }
                    else
                    {
                        StartButton.Text = @"Start";
                        KeepRunning = false;
                    }
                }
                    break;
                case "SendButton":
                {
                    _sendEventMessage();
                }
                    break;
                case "SendExceptionButton":
                {
                    var exceptionMessage = "Sending sample exception message for telemetry tracking.";
                    FireMessage(exceptionMessage);
                    Telemetry.Instance.TrackException(new Exception(exceptionMessage));
                }
                    break;
            }
        }

        /// <summary>
        /// Send a random event message.
        /// </summary>
        private void _sendEventMessage()
        {
            var eventHubClient = EventHubClient.CreateFromConnectionString(EventHubConnectionString, EventHubName);

            try
            {
                var message = $"{DateTime.Now} > Sending message: {Guid.NewGuid().ToString()}";
                FireMessage(message);
                Telemetry.Instance.TrackEvent(message);
                eventHubClient.Send(new EventData(Encoding.UTF8.GetBytes(message)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(Formatting.FormatException(ex));
            }
        }

        /// <summary>
        /// Listen for messages.
        /// </summary>
        private void _runListener()
        {
            try
            {
                string eventProcessorHostName = Guid.NewGuid().ToString();
                var storageConnectionString = $"DefaultEndpointsProtocol=https;AccountName={StorageAccountName};AccountKey={StorageAccountKey}";
                EventProcessorHost eventProcessorHost = new EventProcessorHost(
                    eventProcessorHostName,
                    EventHubName,
                    EventHubConsumerGroup.DefaultGroupName,
                    EventHubConnectionString,
                    storageConnectionString);

                FireMessage("Registering EventProcessor...");
                var options = new EventProcessorOptions();
                options.ExceptionReceived += (sender, e) => { MessageBox.Show(Formatting.FormatException(e.Exception)); };
                eventProcessorHost.RegisterEventProcessorAsync<SimpleEventProcessor>(options).Wait();

                while (KeepRunning)
                {
                    Thread.Sleep(1000);
                }
                eventProcessorHost.UnregisterEventProcessorAsync().Wait();
            }
            catch (Exception e)
            {
                MessageBox.Show(Formatting.FormatException(e));
            }
            
        }
    }
}
