﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DockerWebApiDemo.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DockerWebApiDemo.Controllers
{
    [Route("api")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        /// <summary>
        /// Authorize and Capture a request in one operation.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("authandcapture")]
        [HttpPost]
        public AuthAndCaptureResponse AuthAndCapture(AuthAndCaptureRequest request)
        {
            return new AuthAndCaptureResponse(request)
            {
                TotalAmountCaptured = request.AuthRequestAmount,
                RequestResult = AuthRequestResult.Success,
                ImpactedAccounts = new List<Account>(request.DrainOrder)
            };
        }
    }
}