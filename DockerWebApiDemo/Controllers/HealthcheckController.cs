﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DockerWebApiDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HealthcheckController : ControllerBase
    {
        /// <summary>
        /// Do a healthcheck
        /// </summary>
        [HttpGet]
        public string Healthcheck()
        {
            return "Healthcheck was successful.  Service up and running.";
        }
    }
}