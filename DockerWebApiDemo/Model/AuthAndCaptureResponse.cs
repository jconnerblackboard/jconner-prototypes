﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DockerWebApiDemo.Model
{
    /// <summary>
    /// Response to an auth request.
    /// </summary>
    public class AuthAndCaptureResponse
    {/// <summary>
     /// The unique identifier for the request.
     /// </summary>
        public Guid RequestId { get; set; }

        /// <summary>
        /// The unique identifier for the authorization.
        /// </summary>
        public Guid AuthorizationId { get; set; }

        /// <summary>
        /// The unique identifier for the source institution.
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// The unique identifier for the account holder associated with the request.
        /// </summary>
        public Guid AccountHolderId { get; set; }

        /// <summary>
        /// The requested amount.
        /// </summary>
        public decimal AuthRequestAmount { get; set; }

        /// <summary>
        /// The result of the request.
        /// </summary>
        public AuthRequestResult RequestResult { get; set; }

        /// <summary>
        /// List of accounts in which the <see cref="AuthRequestAmount"/> is applied in order until the amount is satisfied.  
        /// Only accounts that were impacted by the capture process are present.
        /// </summary>
        public List<Account> ImpactedAccounts { get; set; } = new List<Account>();

        /// <summary>
        /// Total amount that was captured.
        /// </summary>
        public decimal TotalAmountCaptured { get; set; }

        /// <summary>
        /// Parameterized constructor requiring an AuthAndCaptureRequest.
        /// </summary>
        /// <param name="request">The request.</param>
        public AuthAndCaptureResponse( AuthAndCaptureRequest request)
        {
            RequestId = request.RequestId;
            AuthorizationId = request.AuthorizationId;
            InstitutionId = request.InstitutionId;
            AccountHolderId = request.AccountHolderId;
            AuthRequestAmount = request.AuthRequestAmount;
        }
    }
}
