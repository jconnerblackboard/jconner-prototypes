﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DockerWebApiDemo.Model
{
    /// <summary>
    /// Auth request.
    /// </summary>
    public class AuthAndCaptureRequest
    {
        /// <summary>
        /// The unique identifier for the request.
        /// </summary>
        public Guid RequestId { get; set; } = Guid.NewGuid();

        /// <summary>
        /// The unique identifier for the authorization.
        /// </summary>
        public Guid AuthorizationId { get; set; }

        /// <summary>
        /// The unique identifier for the source institution.
        /// </summary>
        public Guid InstitutionId { get; set; }

        /// <summary>
        /// The unique identifier for the account holder associated with the request.
        /// </summary>
        public Guid AccountHolderId { get; set; }

        /// <summary>
        /// The requested amount.
        /// </summary>
        public decimal AuthRequestAmount { get; set; }

        /// <summary>
        /// List of accounts in which the <see cref="AuthRequestAmount"/> is applied in order until the amount is satisfied.
        /// </summary>
        public List<Account> DrainOrder { get; set; } = new List<Account>();
    }
}
