﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DockerWebApiDemo.Model
{
    /// <summary>
    /// Container class for an account
    /// </summary>
    public class Account
    {
        /// <summary>
        /// The account type id.
        /// </summary>
        public AccountType AccountTypeId { get; set; }
        
        /// <summary>
        /// Maximum allowed amount for this account.
        /// </summary>
        public decimal MaxAllowedForAccount { get; set; }

        /// <summary>
        /// Partial amount allowed.
        /// </summary>
        public bool PartialAmountAllowed { get; set; }
    }
}
