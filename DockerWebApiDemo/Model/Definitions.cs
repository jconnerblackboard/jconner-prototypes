﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DockerWebApiDemo.Model
{
    /// <summary>
    /// Enumeration for account types.
    /// </summary>
    public enum AccountType
    {
        Unknown = 0,
        Type1 = 1,
        Type2 = 2
    }

    /// <summary>
    /// Enumeration of authorization request results.
    /// </summary>
    public enum AuthRequestResult
    {
        Success = 0,
        InstitutionNotFound = 1,
        AccountHolderNotFound = 2,
        InsufficientFunds = 3
    }

    public class Definitions
    {
    }
}
