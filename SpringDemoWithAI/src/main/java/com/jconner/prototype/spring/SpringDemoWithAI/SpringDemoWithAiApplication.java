package com.jconner.prototype.spring.SpringDemoWithAI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDemoWithAiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDemoWithAiApplication.class, args);
	}

}
