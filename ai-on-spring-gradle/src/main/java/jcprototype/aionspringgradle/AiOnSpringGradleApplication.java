package jcprototype.aionspringgradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AiOnSpringGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(AiOnSpringGradleApplication.class, args);
	}

}
