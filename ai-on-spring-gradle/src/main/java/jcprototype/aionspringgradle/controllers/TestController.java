package jcprototype.aionspringgradle.controllers;

import com.microsoft.applicationinsights.TelemetryClient;
import com.microsoft.applicationinsights.TelemetryConfiguration;
import com.microsoft.applicationinsights.telemetry.Duration;
import jcprototype.aionspringgradle.models.TestWidgetRequest;
import jcprototype.aionspringgradle.models.TestWidgetResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sample")
public class TestController {

    //@Autowired
    //TelemetryClient telemetryClient;

    @GetMapping("/hello")
    public String hello() {

        TelemetryConfiguration config = TelemetryConfiguration.createDefault();
        TelemetryClient telemetryClient = new TelemetryClient(config);

        //track a custom event
        telemetryClient.trackEvent("Sending a custom event...");

        //trace a custom trace
        telemetryClient.trackTrace("Sending a custom trace....");

        //track a custom metric
        telemetryClient.trackMetric("custom metric", 1.0);

        //track a custom dependency
        telemetryClient.trackDependency("SQL", "Insert", new Duration(0, 0, 1, 1, 1), true);

        return "hello";
    }

    @RequestMapping(value = "/widget", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public TestWidgetResponse testWidget(@RequestBody TestWidgetRequest request){
        TestWidgetResponse response = new TestWidgetResponse(request);
        return response;
    }
}
