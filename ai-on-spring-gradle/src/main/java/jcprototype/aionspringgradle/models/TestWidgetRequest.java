package jcprototype.aionspringgradle.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestWidgetRequest {
    private String requestId;
    private String name;
    private String valueOne;
    private String valueTwo;
    public final String type = "TestWidgetRequest";
}
