package jcprototype.aionspringgradle.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestWidgetResponse {
    private String requestId;
    private String name;
    private String valueOne;
    private String valueTwo;
    public final String type = "TestWidgetResponse";

    public TestWidgetResponse(TestWidgetRequest request)
    {
        requestId = request.getRequestId();
        name = request.getName();
        valueOne = request.getValueOne();
        valueTwo = request.getValueTwo();
    }
}
